//alert("hi!")

function Pokemon(name, lvl, hp){
	this.name = name;
	this.lvl = lvl;
	this.hp = hp;
	this.atk = lvl*2;
	this.special = lvl*3;


	//Pokemon attacks
	this.tackle = function(target){
		if (this.hp < 10){
			console.log(`${this.name} is unable to battle.`)
			return;
		} else {
		console.log(`${this.name} tackled ${target.name}`)
		console.log(`${target.name} health is now reduced by ${this.atk} points, now at ${target.hp-this.atk}.`)
		target.hp = target.hp-this.atk;
		if (target.hp < 10){
			console.log(`${target.name} is unable to battle.`)
		}}
	};

	//Pokemon loses
	this.faint = function(){
		console.log(`${this.name} fainted`)
	};

	//Special Skill
	this.spl_atk = function(target){
		if (this.hp < 10){
			console.log(`${this.name} is unable to battle.`)
			return;
		} else {
		console.log(`${this.name} used special skill to ${target.name}`)
		console.log(`${target.name} health is now reduced by ${this.special} points, now at ${target.hp-this.special}.`)
		target.hp = target.hp-this.special;
		if (target.hp < 10){
			console.log(`${target.name} is unable to battle.`)
		}}
	};

};

//declare pokemon
let charmander = new Pokemon("Charmander", 3, 30)
let aerodactyl = new Pokemon("Aerodactyl", 5, 50)
let machoke = new Pokemon("Machoke", 8, 70)

console.log("You sumoned " + charmander.name);
console.log("You summoned " + aerodactyl.name);
console.log("The battle of " + charmander.name + " and " + aerodactyl.name + " begins");

//attacks
charmander.tackle(aerodactyl);
charmander.tackle(aerodactyl);
charmander.tackle(aerodactyl);
charmander.tackle(aerodactyl);
charmander.tackle(aerodactyl);


//special skill
charmander.spl_atk(aerodactyl);

console.log("New Match begins.")

//attacks
aerodactyl.tackle(charmander);
aerodactyl.tackle(charmander);
aerodactyl.tackle(charmander);
aerodactyl.tackle(charmander);
aerodactyl.tackle(charmander);
charmander.faint();

//attacks
machoke.tackle(aerodactyl);
machoke.tackle(aerodactyl);






