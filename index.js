/*alert("Hello")*/

/*
Javascript objects 
similar to an array containing multiple values 
properties:value

let objectName = {
	key : value,
	key : (function),
}
*/

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log(cellphone);
console.log(typeof cellphone);

/*
function objectName (keya, keyb){
	this.keya = keya;
	this.keyb = keyb;
}
*/

//object constructor
function Laptop(name, manufactureDate){
	this.name = name;
	this.releaseDate = manufactureDate;
	/*console.log(`The laptop name is ${name} and manufactureDate at ${manufactureDate}`);*/
	//there is no return so the purpose is to create an object
}


let laptop = new Laptop("Lenovo", 2008);
console.log(laptop);
//new keyword used to create new objects (new container for function)

let laptop1 = new Laptop("Asus", 2010);
console.log(laptop1);

let computer = {};

//Laptop("huawei", 2019);

let machines = [laptop, laptop1];
console.log(machines[0].name);
console.log(machines[1].name);
console.log(machines[0]["releaseDate"]);
console.log(machines[0].releaseDate);

/*
Initializing/Adding/Deleting/Reassigning Object Properties

*/

let car = {};
car.name = "Honda";
console.log(car);

car["manufactureDate"] = 2019;
console.log(car);

//delete object properties

delete car["name"];
console.log(car);

//car.name; dont work
car.brand = "Tesla";
car.model = "Type A";
car.color = "Bluish Gray";
car.location = "USA";
console.log(car);

console.log(car.name);//undefined doesnt exist

//Reassigning properties

car.manufactureDate = 2021;
console.log(car);

/*
Object methods
method is a function 
methods are defined based on what an object is capable of doing and how it should work
*/

let person = {
	name: "Jin",
	//talk is the function name as property
	talk: function(){
			console.log(`Hello my name is ${this.name}`);
	}
};


person.talk();
console.log(person.name);
//undefined not working console.log(person.talk());

let friend ={
	firstName: "Bam",
	lastName: "Jeon",
	address: {
		city: "Caloocan",
		country: "Philippines"
	},
	emails: ["bam@mail.com", "jeonbam@gmail.com"],
	introduce: function(){
		console.log(`Hello my name is ${this.firstName} ${this.lastName} I live in ${this.address.city}. My personal email is ${this.emails[0]}`)
	}
};

//you can not call array, objects, without this/ properties or index included

friend.introduce();

let myPokemon = {
	name: "Bulbasur",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log(`${this.name} tackled another pokemon`)
		console.log(`target pokemon's health reduced by ${this.attack} points`)
	},
	faint: function(){
		console.log(`${this.name} fainted`)
	}
}

myPokemon.tackle();
myPokemon.faint();

/*
Creating object with an object constructor
*/

function Pokemon(name, lvl, hp){
	//properties
	this.name = name;
	this.lvl = lvl;
	this.health = hp * 2;
	this.attack = lvl;
	//methods
	this.tackle = function(target){
		console.log(`${this.name} tackled ${this.target.name}`)
		console.log(`Target pokemon health is now reduced`)
		console.log(target.name);
		console.log(this.name);
	this.faint = function(){
		console.log(`${this.: value,} fainted`)
	}
	}
};

let pikachu = new Pokemon("Pikachu")
let ratata = new Pokemon("Ratata")

pikachu.tackle(ratata);
ratata.tackle(pikachu);






